<?php

require_once __DIR__ . '/vendor/autoload.php';

$worker = new DennyEmulator\Denny();

for ($i=0; $i < 10; $i++) {
  $worker->doSomething();
  sleep(5);
}
