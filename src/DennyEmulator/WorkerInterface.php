<?php

namespace DennyEmulator;

interface WorkerInterface
{
  public function doSomething();
}
