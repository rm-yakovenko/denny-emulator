<?php

namespace DennyEmulator;

use DennyEmulator\WorkerInterface;

/**
 *
 */
class Denny implements WorkerInterface
{
  private function readCensorNetUa()
  {
    echo "Denny is reading censort.net.ua" . PHP_EOL;
  }

  private function readSkype()
  {
    echo "Denny is reading Skype" . PHP_EOL;
  }

  public function doSomething()
  {
    if (rand() % 2 == 0) {
      $this->readCensorNetUa();
    } else {
      $this->readSkype();
    }
  }
}
